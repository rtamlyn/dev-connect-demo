# Dev Connect Demo

## Application Plan

1. Setup signup with initial user profile (go to dashboard)
1. setup login
1. Set up dashboard with editing form etc
1. List all developers for guest user
1. Set up posts for only logged in users
1. Setup catigories i.e. skills for the developers
1. Setup mapping program - give mapquest or openstreetmap a try

## Setup

## Database CLI

- Mongo

```bash
MONGO_URL='mongodb+srv://chat-dev-server:W_%3BjB%5Bw%7B%7DD2%2BRkQK@dev-vejwg.mongodb.net/test?retryWrites=true'
mongo $MONGO_URL
show dbs
use dcn-demo
show collections
db['chat-users'].drop()
```

### ng g

```bash
ng g m core -d
ng g c core/Navbar -d
ng g c core/Home -d
ng g c core/NotFound -d

# maybe this should be a routing module
ng g m auth -d
ng g c auth/login -d
ng g c auth/signup -d

# developers routing module
ng g m developers --routing -d
ng g c developers/profiles -d
ng g c developers/profile -d
ng g c developers/Dashboard -d
ng g c developers/create-profile -d
ng g c developers/CreateProfile -d
ng g c developers/AddExperience -d
ng g c developers/AddEducation -d
ng g c developers/posts -d
ng g c developers/discuss -d

# developers services
ng g s developers/services/profile -d

# ngrx
npm install @ngrx/schematics -D
npm install @ngrx/{store,effects,entity,store-devtools}
npm install @ngrx/{store,effects,store-devtools} # skip entity for now

ng g @ngrx/schematics:store State --root --statePath state --module app.module.ts -d
ng g @ngrx/schematics:feature state/auth --flat false --reducers index.ts -d
ng g @ngrx/schematics:feature state/developer --flat false --reducers index.ts -d

```

## Resume Notes copied below HERE

## Code Development

```bash
yarn start # no seperate server yet
```

## Setup Resume

### Yarn Commands

```bash
yarn install # setup install
yarn upgrade  # same as npm update
yarn outdated # same
yarn add @angular-devkit/build-angular@latest # same as npm install
yarn audit
npm install yarn@latest -g # to update yarn
# dont use the following; use above
curl --compressed -o- -L https://yarnpkg.com/install.sh | bash # to update yarn DARN installed locally GRRRR.
```

### Generate RSA Keys

```bash
cd server/private
ssh-keygen -t rsa -b 1024 -f id_rsa1024
ssh-keygen -t rsa -b 768 -f id_rsa768
# create pem format
ssh-keygen -f id_rsa768.pub -e -m pem > id_rsa768.pem.pub
```

### Add Google Maps

Be sure to add googlemaps to the types array in tsconfig.app.json and tsconfig.spec.json

Add mocks folder with mock google api

Add files to karma.conf.js

### Install latest gcc

```bash
sudo apt install g++
```

### ng g Resume

```bash
ng new resume --routing
yarn add @angular/material @angular/cdk  @angular/animations hammerjs

ng g m resume
ng g c resume/resume -d
ng g m core -d
ng g c core/MainDashboard -d
ng g c core/Navbar -d
ng g s resume/services/resume -d
ng g c resume/education -d
ng g c resume/projects -d
ng g c resume/work-experience -d
ng g c resume/header -d
yarn add @types/googlemaps --dev
ng g c resume/google-map -d
ng g m auth -d
ng g c auth/login -d
ng g c auth/signup -d
ng g c resume/add-resume -d
ng g @angular/material:address-form resume/bio-form -d
ng g c resume/forms/education-form -d
ng g c resume/forms/work-history-form -d
ng g c resume/forms/projects-form -d
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
