# DevConnectDemo

## Setup

1. Clone repo
2. mkdir keys

- Create jwt keys

```bash
mkdir server/keys
cd server/keys
ssh-keygen -t rsa -b 1024 -f jwt-key # with no passphrase
# use this format of the public key in the server
ssh-keygen -f jwt-key.pub -e -m pem > jwt-key.pem.pub # creates pem format
```

- Create secure server key and self-signed certificate

```bash
# create openssl-custom.cnf

[req]
default_bits = 2048
prompt = no
default_md = sha256
x509_extensions = v3_req
distinguished_name = dn

[dn]
C = US
ST = NC
L = Apex
O = Web Development
OU = IT Department
emailAddress = memorychips@yahoo.com
CN = localhost

[v3_req]
subjectAltName = @alt_names

[alt_names]
DNS.1 = *.localhost
DNS.2 = localhost
# end cnf file
cd keys
openssl req \
    -newkey rsa:2048 \
    -x509 \
    -nodes \
    -keyout server.key \
    -new \
    -out server.crt \
    -config ./openssl-custom.cnf \
    -sha256 \
    -days 365
```

3. Copy .env-example to .env

- Replace environment vars as per instructions
