import { NextFunction } from 'express'
import { RequestApp, ResponseApp } from '../../express-app'

export function checkCsrfToken(req: RequestApp, res: ResponseApp, next: NextFunction) {
  const csrfCookie = req.cookies['XSRF-TOKEN']
  const csrfHeader = req.headers['x-xsrf-token']
  if (csrfCookie && csrfHeader && csrfCookie === csrfHeader) {
    next()
  } else {
    console.log('Missing csrf cookie')
    res.sendStatus(403)
  }
}
