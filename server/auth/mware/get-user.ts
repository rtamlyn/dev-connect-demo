import { getUserFromJwt } from '../security'
import { NextFunction, Request } from 'express'
import { RequestApp, ResponseApp } from '../../express-app'
// TODO: is there a better way than this package?
import { parse } from 'cookie'

// TODO: use getUserFromReq instead of duplicate code
export function addUserToRes(req: RequestApp, res: ResponseApp, next: NextFunction) {
  const jwt = req.cookies.SESSIONID
  if (jwt) {
    getUserFromJwt(jwt)
      .then(user => {
        if (user) res.locals.user = user
        next()
      })
      .catch(_err => {
        console.log(`Did not find user in request. Continuing anyway for now...`)
        next()
      })
  } else {
    next()
  }
}

// TODO: should this use RequestApp?
export function getUserFromReq(req: Request) {
  const cookies = parse(req.headers.cookie || '')
  const jwt = cookies.SESSIONID
  return getUserFromJwt(jwt)
}
