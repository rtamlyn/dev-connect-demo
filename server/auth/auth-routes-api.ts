import express from 'express'
import { createUser, login, logout, getJwtUser, getUserByEmail, deleteUser } from './auth-api'
import { checkIfAuthenticated, checkForRelogin } from './mware/authentication'
import { checkIfAuthorized } from './mware/authorization'
import { checkCsrfToken } from './mware/csrf'

export const authRouter = express.Router()

// authRouter.use((req, _res, next) => {
//   console.log(`${req.url} Time: `, Date.now())
//   next()
// })

authRouter
  .post('/signup', createUser)
  .post('/login', login)
  .post('/logout', checkIfAuthenticated, checkCsrfToken, logout)
  .get('/user-me', checkForRelogin, getJwtUser) // used by relogin so cannot check csrftoken
  .get('/user/:email', checkIfAuthenticated, checkCsrfToken, getUserByEmail) // TODO: Test where this is used
  // .put('/user/:id', checkIfAuthenticated, saveUser)
  .delete(
    '/user/:email',
    checkIfAuthenticated,
    checkCsrfToken,
    checkIfAuthorized(['ADMIN']),
    deleteUser
  )
