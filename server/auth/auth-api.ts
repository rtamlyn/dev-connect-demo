import { User, SignUpInfo, Credentials } from '../database/user-db/user'
import argon2 from 'argon2'
import { validatePassword } from './password-validation'
import { createCsrfToken, createSessionToken } from './security'

import { serverConfig } from '../server-config'
import { UserDatabase, fromPromise } from '../database/user-db/user-db'

import { RequestApp, ResponseApp } from '../express-app'
import { switchMap, take } from 'rxjs/operators'
import { Observable, throwError } from 'rxjs'

export function createUser(req: RequestApp, res: ResponseApp) {
  const signUpInfo: SignUpInfo = req.body
  signUpInfo.email = signUpInfo.email.toLowerCase()
  // FIXME: validate signUpInfo
  const errors: string[] = validatePassword(signUpInfo.passwordGroup.password)
  if (errors.length > 0) {
    res.status(406).send(JSON.stringify(errors))
  } else {
    createUserAndSession(req, res, signUpInfo)
  }
}

function setRoles(email: string): string[] {
  let roles = ['STUDENT']
  if (email.toLowerCase() === 'robert.tamlyn@gmail.com') {
    roles = ['STUDENT', 'ADMIN']
  }
  if (email.toLowerCase() === 'fsYB4k9qHaB95NpN@gmail.com'.toLowerCase()) {
    roles = ['STUDENT', 'ADMIN']
  }
  return roles
}

// interface MyUser extends User {
//   created: number
// }
function createUserAndSession(req: RequestApp, res: ResponseApp, signUpInfo: SignUpInfo) {
  // FIXME: validate signup info
  argon2.hash(signUpInfo.passwordGroup.password).then((passwordDigest: string) => {
    const user: User<null> = {
      _id: null,
      email: signUpInfo.email,
      userName: signUpInfo.userName,
      roles: setRoles(signUpInfo.email),
      avatarUrl: signUpInfo.avatarUrl || '',
      created: Date.now()
    }
    const db: UserDatabase = req.app.locals.dbs.userDb
    // const chatDb: ChatDatabase = req.app.locals.dbs.chatDb
    db.createUser$(user, passwordDigest)
      // .pipe(
      //   catchError(err => {
      //     // console.error(`Unable to create user: ${err}`)
      //     res.status(406).json({ errors: [err] })
      //     return throwError(err)
      //   }),
      //   switchMap(newUser => fromPromise(sendSuccess(res, newUser)))
      // )
      .subscribe(
        result => {
          if (result.success) {
            sendSuccess(res, result.user)
          } else {
            res.status(406).json({ errors: result.errors })
          }
        },
        err => console.log(`Server error creating user: ${err}`),
        () => console.log(`Chat user create user and session complete`)
      )
  })
}

export function getJwtUser(req: RequestApp, res: ResponseApp) {
  const userNotFound = {
    _id: undefined,
    email: undefined,
    userName: 'anonymous'
  }
  const jwtUser: User<string> = res.locals.user
  if (!!jwtUser && jwtUser._id) {
    const db: UserDatabase = req.app.locals.dbs.userDb
    db.getUserById$(jwtUser._id).subscribe(
      user => {
        if (!user) return res.status(403).json(userNotFound)
        res.status(200).json(user)
      },
      err => {
        res.status(403).json(`Error trying to get jwt user: ${err}`)
      }
    )
  } else {
    res.status(200).json(userNotFound)
  }
}

// export function getUser(req: RequestApp, res: ResponseApp) {
//   const db: UserDatabase = req.app.locals.dbs.userDb
//   if (req.params._id) {
//     const userId = req.params._id
//     // db.getUserById(userId).then(user => {
//     db.getUserById(userId).then(user => {
//       if (!user) return res.status(403).json({ error: `User with id: ${userId} not found` })
//       res.status(200).json(user)
//     })
//   } else {
//     res.status(403).json('Error trying to get user')
//   }
// }

export function getUserByEmail(req: RequestApp, res: ResponseApp) {
  const db: UserDatabase = req.app.locals.dbs.userDb
  if (req.params.email) {
    const userId = req.params.email
    db.getUserByEmail$(userId).subscribe(user => {
      if (!user) return res.status(403).json({ error: `User with id: ${userId} not found` })
      res.status(200).json(user)
    })
  } else {
    res.status(403).json('Error trying to get user')
  }
}

export function deleteUser(req: RequestApp, res: ResponseApp) {
  const db: UserDatabase = req.app.locals.dbs.userDb
  const email = req.params.email
  db.deleteUser$(email).subscribe(
    success => {
      if (success) {
        res.status(200).json({ success, reason: `user with email ${email} deleted` })
      } else {
        res
          .status(403)
          .json({ success, reason: `user with email ${email} not found and not deleted` })
      }
    },
    err => {
      console.error(`Error deleting user: ${err}`)
    }
  )
}

async function sendSuccess(res: ResponseApp, user: User) {
  const sessionToken = await createSessionToken(user)
  const csrfToken = await createCsrfToken()
  res.cookie('SESSIONID', sessionToken, { maxAge: serverConfig.TOKEN_AGE_MS, httpOnly: true })
  res.cookie('XSRF-TOKEN', csrfToken)
  res.status(200).json(user)
  return !!user
}

function verifyPassword$(email: string, password: string, db: UserDatabase): Observable<boolean> {
  return db
    .getUserPwdDigest$(email)
    .pipe(switchMap(digest => fromPromise(argon2.verify(digest, password))))
}

export function login(req: RequestApp, res: ResponseApp) {
  const db: UserDatabase = req.app.locals.dbs.userDb
  const credentials: Credentials = req.body
  const password: string = credentials.password
  const email: string = credentials.email
  verifyPassword$(email, password, db)
    .pipe(
      switchMap(success => {
        if (!success) {
          return throwError(`Password does not match`)
        } else {
          return db.getUserByEmail$(email)
        }
      }),
      take(1)
    )
    .subscribe(
      user => sendSuccess(res, user),
      // err => res.status(406).json({ errors: [err] })
      err => {
        console.log(err)
        const e = `User with ${email} and that password combination not found`
        res.status(406).send(`${e}`)
      }
      // () => console.log(`user login complete`)
    )
}

export function logout(_req: RequestApp, res: ResponseApp) {
  res.clearCookie('SESSIONID')
  res.clearCookie('XSRF-TOKEN')
  res.status(200).json({ success: true })
}
