// import fs from 'fs'
// import dotenv from 'dotenv'
// dotenv.config()

// const TOKEN_AGE_MS = 1000 * 60 * 60 * Number(process.env.TOKEN_AGE_HRS)
// const RSA_PRIVATE_KEY = process.env.RSA_PRIVATE_KEY || fs.readFileSync('./keys/jwt-key')
// const RSA_PUBLIC_KEY = process.env.RSA_PUBLIC_KEY || fs.readFileSync('./keys/jwt-key.pem.pub')
// const SESSION_DURATION_SECS = Math.round(TOKEN_AGE_MS / 1000)
// const MONGO_URL = process.env.MONGO_URL || 'mongo url should be here'
// const MONGO_DATABASE = process.env.MONGO_DATABASE || 'mongo database should be here'
// const PORT = process.env.PORT || 0
// const HOST_URL = process.env.HOST_URL || 'host url should be here'

// export const config = {
//   RSA_PRIVATE_KEY,
//   RSA_PUBLIC_KEY,
//   MONGO_URL,
//   MONGO_DATABASE,
//   HOST_URL,
//   PORT,
//   TOKEN_AGE_MS,
//   SESSION_DURATION_SECS
// }
// console.log(`session duration is set to: ${config.SESSION_DURATION_SECS} secs`)
// Command line settings
import { ClientOpts } from 'redis'
import commandLineArgs from 'command-line-args'

import dotenv from 'dotenv'

import { configure, getLogger } from 'log4js'

function setupLogging(useLogLineNumbers = false) {
  // trace debug info warn error fatal
  // const level = 'error'
  const level = 'warn'
  let enableCallStack = false
  let appenders = ['console']
  if (useLogLineNumbers) {
    // TODO: make this a commanmd line arg
    enableCallStack = true
    appenders = ['consoleWithLineNum']
  }
  configure({
    appenders: {
      console: { type: 'console' },
      consoleWithLineNum: {
        type: 'console',
        layout: { type: 'pattern', pattern: '%[[%d] [%5.5p]%] %f{1}:%l %m' }
      }
    },
    categories: {
      default: { appenders, level: 'trace', enableCallStack },
      server: { appenders, level, enableCallStack },
      'server-config': { appenders, level: 'info', enableCallStack },
      'connect-dbs': { appenders, level: 'info', enableCallStack },
      'create-redis-client': { appenders, level: 'error', enableCallStack },
      'redis-categories': { appenders, level, enableCallStack },
      security: { appenders, level, enableCallStack },
      'create-mongo-client': { appenders, level, enableCallStack },
      'mongo-shopping-cart': { appenders, level, enableCallStack },
      'mongo-products': { appenders, level, enableCallStack }
    }
  })
}

dotenv.config()

const optionDefinitions = [
  { name: 'secure', type: Boolean },
  { name: 'prod', type: Boolean },
  { name: 'useRedisCategories', type: Boolean },
  { name: 'useRedisChatDb', type: Boolean },
  { name: 'useRedisUserDb', type: Boolean },
  { name: 'logLineNumbers', type: Boolean },
  { name: 'useMemChatDb', type: Boolean }
]
export const options = commandLineArgs(optionDefinitions)
function getOption(option: string, defOption: any) {
  return options[option] ? options[option] : defOption
}

const useRedisCategories: boolean = getOption('useRedisCategories', false)
const useRedisChatDb: boolean = getOption('useRedisChatDb', false)
const useMemChatDb: boolean = getOption('useMemChatDb', false)
const useRedisUserDb: boolean = getOption('useRedisUserDb', false)
const logLineNumbers: boolean = getOption('logLineNumbers', false)

setupLogging(logLineNumbers)

const log = getLogger('server-config')

// log.info = console.log
log.info(`Using redis user db: ${useRedisUserDb}`)
log.info(`Using redis chat db: ${useRedisChatDb}`)
log.info(`Using redis categories db: ${useRedisCategories}`)

const MONGO_DATABASE = process.env.MONGO_DATABASE || 'data-base-name-here'
const MONGO_URL = process.env.MONGO_URL || 'atlas-mongo-url-here'
const HOST_URL = process.env.HOST_URL || 'localhost'
const PORT = process.env.PORT || '9000'
const TOKEN_AGE_MS = Number(process.env.TOKEN_AGE_MS) || 24 * 60 * 60 * 1000

export const serverConfig = {
  secure: !!options.secure,
  prod: !!options.prod,
  useMemChatDb,
  useRedisChatDb,
  useRedisUserDb,
  useRedisCategories,
  MONGO_URL,
  MONGO_DATABASE,
  HOST_URL,
  PORT,
  TOKEN_AGE_MS
}

const REDSI_DB_HOST = process.env.REDIS_DB_HOST || 'localhost'
const REDIS_DB_PORT = Number(process.env.REDIS_DB_PORT || 6379)
const REDIS_DB_NUM = process.env.REDIS_DB_NUM || 2
const REDIS_DB_AUTHCODE = process.env.REDIS_DB_AUTHCODE || 'this_should_be_a_secret_authcode'

export const redisClientOptions: ClientOpts = {
  host: REDSI_DB_HOST,
  port: REDIS_DB_PORT,
  auth_pass: REDIS_DB_AUTHCODE,
  db: REDIS_DB_NUM,
  retry_strategy: retryOptions => {
    log.debug(
      `Trying to reconnect: ${retryOptions.attempt} attempt. ${retryOptions.total_retry_time} total retry time.`
    )
    return 5000
  }
}
