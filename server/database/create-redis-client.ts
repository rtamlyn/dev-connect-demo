import { RedisClient, createClient, ClientOpts } from 'redis'
import { getLogger } from 'log4js'
const log = getLogger('create-redis-client')

export function createRedisClient(options: ClientOpts): Promise<RedisClient> {
  return new Promise<RedisClient>((resolve, reject) => {
    const redisClient: RedisClient = createClient(options)

    redisClient.on('reconnecting', _e => {
      log.warn(`Attempting reconnect`)
    })

    redisClient.on('connect', () => {
      log.warn(`Redis client is connected`)
    })

    redisClient.on('ready', () => {
      log.trace(`Connected to redis client: ${options.host}`)
      resolve(redisClient)
    })

    redisClient.on('error', err => {
      log.error(`Redis client error: ${err}`)
      reject(err)
    })
  })
}
