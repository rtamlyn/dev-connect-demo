import { ObjectId } from 'mongodb'

type ID = string | ObjectId

export interface Password {
  _id?: ID
  email: string
  passwordDigest: string
}

// export interface Password<T = string> {
//   _id?: T
//   email: string
//   passwordDigest: string
// }

// Align FE/BE
export interface Credentials {
  email: string
  password: string
  userName?: string
}

export interface SignUpInfo {
  email: string
  passwordGroup: { password: string; confirmPassword: string }
  userName: string
  avatarUrl: string
}

// export interface UserWoId {
//   email: string
//   userName: string
//   roles: string[]
//   avatarUrl: string
// }

export interface User<T = string> {
  _id: T
  email: string
  userName: string
  roles: string[]
  avatarUrl: string
  [key: string]: any
}
// end Align FE/BE
