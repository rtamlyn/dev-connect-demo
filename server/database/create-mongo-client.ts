import { MongoClient } from 'mongodb'
import { getLogger } from 'log4js'
const log = getLogger('create-mongo-client')

export function createMongoClient(mongoUrl: string): Promise<MongoClient> {
  return MongoClient.connect(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true }).then(
    client => {
      log.info(`Mongo client created using ${mongoUrl}`)
      return client
    }
  )
}
