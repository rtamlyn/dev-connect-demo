import express from 'express'

import { MongoClient } from 'mongodb'
import { config } from '../server-config'
import { USER_DB, MongoUserDatabase } from './mongo-users'

export async function connectMongo(app: express.Application) {
  return MongoClient.connect(config.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }).then(mongoClient => {
    app.locals[USER_DB] = new MongoUserDatabase(mongoClient, config.MONGO_DATABASE)
    return mongoClient.isConnected()
  })
}
