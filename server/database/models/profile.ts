export interface Experience {
  title: string
  company: string
  location: string
  from: Date
  to: Date
  current: boolean
  description: string
}

export interface Education {
  school: string
  degree: string
  fieldofstudy: string
  from: Date
  to: Date
  current: boolean
  description: string
  type: string
}
export interface Social {
  youtube: string
  twitter: string
  facebook: string
  linkedin: string
  instagram: string
}
export interface Profile {
  user: any
  company: string
  website: string
  location: string
  status: string
  skills: string[]
  bio: string
  githubusername: string
  experience: Experience[]
  education: Education[]
  social: Social
  date: Date
}
