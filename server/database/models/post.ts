export interface Like<T = string> {
  user: T
}

export interface Comment<T = string> {
  user: T
  text: string
  name: string
  avatar: string
  date: Date
}

export interface Post<T = string> {
  user: T
  text: string
  name: string
  avatar: string
  likes: Like<T>[]
  comments: Comment<T>[]
  date: Date
}
