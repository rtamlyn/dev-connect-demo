import { serverConfig, redisClientOptions } from './server-config'

// necessary imports from bldg25 chat server package
// import {
//   ChatRedisDatabase,
//   ChatMongoDatabase,
//   createChatMemoryDatabase as createChatMemDb,
//   ChatDatabase
// } from 'bldg25-chat-server'

// import { RedisCategoryDatabase } from './database/categories-db/redis-categories'
import { RedisUserDatabase } from './database/user-db/redis-users'
import { createRedisClient } from './database/create-redis-client'
import { createMongoClient } from './database/create-mongo-client'
import { MongoUserDatabase } from './database/user-db/mongo-users'
// import { MongoProductDatabase } from './database/mongo-products'
// import { MongoCategoryDatabase } from './database/categories-db/mongo-categories'
// import { MongoShoppingCartDatabase } from './database/mongo-shopping-cart'
// import { CategoryDatabase } from './database/categories-db/categories-db'
import { MongoClient } from 'mongodb'
import { RedisClient } from 'redis'
import { ExpressApp } from './express-app'
import { UserDatabase } from './database/user-db/user-db'

import { getLogger } from 'log4js'
const log = getLogger('connect-dbs')

export interface Databases {
  userDb: UserDatabase
  // chatDb: ChatDatabase
  // productDb: MongoProductDatabase
  // catagoryDb: CategoryDatabase
  // shoppingCartDb: MongoShoppingCartDatabase
}

interface Clients {
  mongoClient: MongoClient | null
  redisClient: RedisClient | null
}

export function createClients(): Promise<[MongoClient | null, RedisClient | null]> {
  const dbConnections: [Promise<MongoClient | null>, Promise<RedisClient | null>] = [
    createMongoClient(serverConfig.MONGO_URL),
    createRedisClient(redisClientOptions)
  ]
  return Promise.all(dbConnections)
}

// export function createChatMongoDatabase(client: MongoClient): ChatDatabase {
//   return new ChatMongoDatabase(client, serverConfig.MONGO_DATABASE)
// }
// export function createChatRedisDatabase(client: RedisClient): ChatDatabase {
//   return new ChatRedisDatabase(client)
// }
// export function createChatMemoryDatabase(): ChatDatabase {
//   return createChatMemDb()
// }

// function getChatDatabase(clients: Clients): ChatDatabase {
//   let chatDb: ChatDatabase
//   if (serverConfig.useRedisChatDb && clients.redisClient) {
//     log.info(`Using redis chat database`)
//     chatDb = createChatRedisDatabase(clients.redisClient)
//   } else if (!serverConfig.useMemChatDb && clients.mongoClient) {
//     log.info(`Using chat mongo database`)
//     chatDb = new ChatMongoDatabase(clients.mongoClient, serverConfig.MONGO_DATABASE)
//   } else {
//     log.info(`Using chat memory database`)
//     chatDb = createChatMemoryDatabase()
//   }
//   return chatDb
// }

function getUserDatabase(clients: Clients): UserDatabase {
  let userDb: UserDatabase
  if (serverConfig.useRedisUserDb && clients.redisClient) {
    log.info(`Using redis user database`)
    userDb = createUserRedisDatabase(clients.redisClient)
  } else if (clients.mongoClient) {
    log.info(`Using mongo userr database`)
    userDb = new MongoUserDatabase(clients.mongoClient, serverConfig.MONGO_DATABASE)
  } else {
    log.level = 'trace'
    log.fatal(`Unable to create user database.`)
    if (serverConfig.useRedisUserDb) {
      log.fatal(`Trying to use redis database`)
    }
    return process.exit(42)
  }
  return userDb
}

export function createUserMongoDatabase(client: MongoClient): UserDatabase {
  return new MongoUserDatabase(client, serverConfig.MONGO_DATABASE)
}
export function createUserRedisDatabase(client: RedisClient): UserDatabase {
  return new RedisUserDatabase(client)
}

export async function connectDatabases(): Promise<Databases> {
  return createClients().then(dbClients => {
    const [mongoClient, redisClient] = dbClients
    if (!(mongoClient && mongoClient.isConnected()) || !(redisClient && redisClient.connected)) {
      throw new Error(`Unable to connect to databases`)
    }
    // const chatDb = getChatDatabase({ mongoClient, redisClient })
    // const catagoryDb = serverConfig.useRedisCategories
    //   ? new RedisCategoryDatabase(redisClient)
    //   : new MongoCategoryDatabase(mongoClient, serverConfig.MONGO_DATABASE)
    // const productDb = new MongoProductDatabase(mongoClient, serverConfig.MONGO_DATABASE)
    // const shoppingCartDb = new MongoShoppingCartDatabase(mongoClient, serverConfig.MONGO_DATABASE)
    const userDb = getUserDatabase({ mongoClient, redisClient })
    // const userDb = new MongoUserDatabase(mongoClient, serverConfig.MONGO_DATABASE)
    const dbs: Databases = {
      userDb
      // chatDb,
      // productDb,
      // catagoryDb,
      // shoppingCartDb
    }
    return dbs
  })
}

export async function addDatabases(app: ExpressApp): Promise<boolean> {
  return connectDatabases()
    .then(dbs => {
      app.locals.dbs = dbs
      return true
    })
    .catch(() => false)
}
