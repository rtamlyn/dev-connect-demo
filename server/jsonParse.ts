export function jsonParse<T>(s: string): Promise<T> {
  return new Promise<T>((resolve, reject) => {
    try {
      resolve(JSON.parse(s) as T)
    } catch (error) {
      reject(error)
    }
  })
}

export function jsonParseNull<T>(s: string): T | null {
  try {
    return JSON.parse(s) as T
  } catch (_error) {
    return null
  }
}

export function jsonParseResolve<T>(
  s: string,
  resolve: (value: T) => void,
  reject: (reason?: any) => void
) {
  const result = jsonParseNull<T>(s)
  if (result) resolve(result)
  else reject(null)
}
