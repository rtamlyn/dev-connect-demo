import { Component } from '@angular/core'

@Component({
  selector: 'dcn-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'dev-connect-demo'
}
