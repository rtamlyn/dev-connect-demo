import { of } from 'rxjs'
import { ActivatedRoute } from '@angular/router'
import { StateService } from './state/state.service'
import { AuthService } from './auth/auth.service'
// import { Router } from '@angular/router'

// class MockRouter {}

export class MockStateService {
  myProfile$ = of(null)
}

export class MockAuthService {
  isLoggedIn$ = of(false)
  me$ = of({})
  loginFailReason$ = of(null)
  signupFailReason$ = of(null)
  returnUrl$ = of('not a real url')
  loginSuccess() {}
  setReturnUrl() {}
  dispatch() {}
}

export class MockActivatedRoute {
  snapshot = {
    routeConfig: {
      path: 'not_a_real_path'
    },
    paramMap: {
      get: () => 'fake_id'
    },
    queryParamMap: {
      get: () => 'fake_return_url'
    }
  }
}

export const providers = [
  // { provide: Router, useClass: MockRouter },
  { provide: ActivatedRoute, useClass: MockActivatedRoute },
  { provide: AuthService, useClass: MockAuthService },
  { provide: StateService, useClass: MockStateService }
]
