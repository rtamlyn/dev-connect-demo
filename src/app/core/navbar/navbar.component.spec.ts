import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { NavbarComponent } from './navbar.component'
import { MaterialModule } from '../../material/material.module'

// import { Router } from '@angular/router'
// import { DomSanitizer } from '@angular/platform-browser'
import { providers } from '../../spec-mock'
import { RouterTestingModule } from '@angular/router/testing'

describe('NavbarComponent', () => {
  let component: NavbarComponent
  let fixture: ComponentFixture<NavbarComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NavbarComponent],
      imports: [MaterialModule, RouterTestingModule],
      providers: [...providers]
    }).compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
