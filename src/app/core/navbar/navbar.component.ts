import { Component, OnInit, OnDestroy } from '@angular/core'
import { Router } from '@angular/router'
import { Subscription } from 'rxjs'
import { User } from '../../auth/user.model'
import { DomSanitizer, SafeStyle } from '@angular/platform-browser'
import { AuthService } from 'src/app/auth/auth.service'

@Component({
  selector: 'dcn-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {
  useImageUrl = true
  imageUrl: SafeStyle
  appUser: User
  private subscriptions: Subscription[] = []
  constructor(
    public authService: AuthService,
    private router: Router,
    private domSanitizer: DomSanitizer
  ) {
    this.subscriptions = [
      this.authService.me$.subscribe(appUser => {
        this.appUser = appUser
        if (appUser.avatarUrl) {
          this.imageUrl = this.sanitize(appUser.avatarUrl)
          this.useImageUrl = true
        } else this.useImageUrl = false
      })
    ]
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe())
  }

  sanitize(imageUrl: string) {
    return this.domSanitizer.bypassSecurityTrustStyle(`url(${imageUrl})`)
  }

  logout() {
    this.authService.startLogout()
  }

  login() {
    this.router.navigate(['/auth/login'], { queryParams: { returnUrl: this.router.url } })
  }
  signup() {
    this.router.navigate(['/auth/signup'], { queryParams: { returnUrl: this.router.url } })
  }
}
