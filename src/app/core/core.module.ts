import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { NavbarComponent } from './navbar/navbar.component'
import { NotFoundComponent } from './not-found/not-found.component'
import { RouterModule } from '@angular/router'
import { HomeComponent } from './home/home.component'
import { MaterialModule } from '../material/material.module'
import { MatMenuModule } from '@angular/material/menu'

@NgModule({
  declarations: [NavbarComponent, NotFoundComponent, HomeComponent],
  imports: [CommonModule, RouterModule, MaterialModule, MatMenuModule],
  exports: [NavbarComponent]
})
export class CoreModule {}
