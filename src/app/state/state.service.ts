import { Injectable } from '@angular/core'
import { State, selectMyProfile } from '.'
import { Store } from '@ngrx/store'

@Injectable({
  providedIn: 'root'
})
export class StateService {
  myProfile$ = this.store.select(selectMyProfile)
  constructor(private store: Store<State>) {
    console.log(`state service constructed.`)
  }
}
