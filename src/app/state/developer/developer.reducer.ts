import { DeveloperActions, DeveloperActionTypes } from './developer.actions'
import { Profile, exampleProfile } from '../../developer/services/profile.service'

export const developerFeatureKey = 'developer'

export interface State {
  myProfile: Profile
}

export const initialState: State = {
  myProfile: exampleProfile
}

export function reducer(state = initialState, action: DeveloperActions): State {
  switch (action.type) {
    case DeveloperActionTypes.LoadDevelopers:
      return state

    default:
      console.log(`Warning: developer default reducer case called: ${action.type}.`)
      return state
  }
}
