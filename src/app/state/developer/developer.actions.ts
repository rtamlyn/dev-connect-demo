import { Action } from '@ngrx/store';

export enum DeveloperActionTypes {
  LoadDevelopers = '[Developer] Load Developers',
  
  
}

export class LoadDevelopers implements Action {
  readonly type = DeveloperActionTypes.LoadDevelopers;
}


export type DeveloperActions = LoadDevelopers;
