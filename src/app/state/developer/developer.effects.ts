import { Injectable } from '@angular/core'
import { Actions, Effect, ofType } from '@ngrx/effects'

import { concatMap } from 'rxjs/operators'
import { EMPTY } from 'rxjs'
import { DeveloperActionTypes, DeveloperActions } from './developer.actions'

@Injectable()
export class DeveloperEffects {
  @Effect()
  loadDevelopers$ = this.actions$.pipe(
    ofType(DeveloperActionTypes.LoadDevelopers),
    /** An EMPTY observable only emits completion. Replace with your own observable API request */
    concatMap(() => EMPTY)
  )

  constructor(private actions$: Actions<DeveloperActions>) {}
}
