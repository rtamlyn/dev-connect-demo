import {
  // ActionReducer,
  ActionReducerMap,
  createSelector,
  MetaReducer,
  createFeatureSelector
} from '@ngrx/store'
import { environment } from '../../environments/environment'
import * as fromDeveloper from './developer/developer.reducer'

export interface State {
  [fromDeveloper.developerFeatureKey]: fromDeveloper.State
}

export const reducers: ActionReducerMap<State> = {
  [fromDeveloper.developerFeatureKey]: fromDeveloper.reducer
}

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : []

const developerState = createFeatureSelector<fromDeveloper.State>(fromDeveloper.developerFeatureKey)
export const selectMyProfile = createSelector(
  developerState,
  (state: fromDeveloper.State) => state.myProfile
)

// const authState = createFeatureSelector<fromAuth.State>('auth')
// export const selectIsLoggedIn = createSelector(
//   authState,
//   // (state: State) => state.auth,
//   (state: fromAuth.State) => state.me._id !== ''
// )

// export const selectLoginFailReason = createSelector(
//   authState,
//   (state: fromAuth.State) => state.loginFailReason
// )

// export const selectSignupFailReason = createSelector(
//   authState,
//   (state: fromAuth.State) => state.signupFailReason
// )

// export const selectMe = createSelector(
//   authState,
//   (state: fromAuth.State) => state.me
// )
