import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { SignupComponent } from './auth/signup/signup.component'
import { NotFoundComponent } from './core/not-found/not-found.component'
import { LoginComponent } from './auth/login/login.component'
import { HomeComponent } from './core/home/home.component'

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'auth/login', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'auth/signup', component: SignupComponent },
  { path: 'auth/register', component: SignupComponent },
  {
    path: 'developers',
    loadChildren: () => import('./developer/developer.module').then(mod => mod.DeveloperModule)
  },
  { path: '**', component: NotFoundComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
