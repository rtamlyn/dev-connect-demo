import { ProfileService } from './services/profile.service'
import { providers as globalProviders } from '../spec-mock'
export class MockProviderService {}

// TODO: inherit from global mock?
export const providers = [
  ...globalProviders,
  { provide: ProfileService, useClass: MockProviderService }
]
