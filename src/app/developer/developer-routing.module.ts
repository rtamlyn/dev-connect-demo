import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { ProfilesComponent } from './profiles/profiles.component'
import { ProfileComponent } from './profile/profile.component'
import { DashboardComponent } from './dashboard/dashboard.component'
import { CreateProfileComponent } from './create-profile/create-profile.component'
import { AddExperienceComponent } from './add-experience/add-experience.component'
import { AddEducationComponent } from './add-education/add-education.component'
import { PostsComponent } from './posts/posts.component'
import { DiscussComponent } from './discuss/discuss.component'

const routes: Routes = [
  { path: 'profile', component: ProfileComponent },
  { path: 'profiles', component: ProfilesComponent },
  { path: 'create-profile', component: CreateProfileComponent },
  { path: 'add-experience', component: AddExperienceComponent },
  { path: 'add-education', component: AddEducationComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'discuss', component: DiscussComponent },
  { path: '', component: ProfilesComponent }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeveloperRoutingModule {}
