import { Component, OnInit, OnDestroy } from '@angular/core'
import { StateService } from 'src/app/state/state.service'
import { DomSanitizer, SafeStyle } from '@angular/platform-browser'
import { Profile } from '../services/profile.service'
import { AuthService } from 'src/app/auth/auth.service'

@Component({
  selector: 'dcn-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  myProfile: Profile
  imageUrl: SafeStyle

  constructor(
    private stateService: StateService,
    private authService: AuthService,
    private domSanitizer: DomSanitizer
  ) {
    this._subscriptions = [
      this.stateService.myProfile$.subscribe(p => {
        this.myProfile = p
      }),
      this.authService.me$.subscribe(p => {
        this.imageUrl = p.avatarUrl
        if (p.avatarUrl) {
          this.imageUrl = this.sanitize(p.avatarUrl)
          // this.imageUrl = 'https://s.gravatar.com/avatar/4d0ad65ea90ab712834fb674a712e75e?s=200'
          // TODO: get better default image url
        } else {
          console.log(`Using default image url`)
          this.imageUrl = 'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200'
        }
      })
    ]
  }

  private _subscriptions = []

  ngOnInit() {}

  ngOnDestroy() {
    this._subscriptions.forEach(s => s.unsubscribe())
  }

  sanitize(imageUrl: string) {
    return this.domSanitizer.bypassSecurityTrustUrl(`${imageUrl}`)
  }
}
