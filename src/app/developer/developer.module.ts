import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'

import { DeveloperRoutingModule } from './developer-routing.module'
import { ProfilesComponent } from './profiles/profiles.component'
import { ProfileComponent } from './profile/profile.component'
import { DashboardComponent } from './dashboard/dashboard.component'
import { CreateProfileComponent } from './create-profile/create-profile.component'
import { AddExperienceComponent } from './add-experience/add-experience.component'
import { AddEducationComponent } from './add-education/add-education.component'
import { PostsComponent } from './posts/posts.component'
import { DiscussComponent } from './discuss/discuss.component'

@NgModule({
  declarations: [
    ProfilesComponent,
    ProfileComponent,
    DashboardComponent,
    CreateProfileComponent,
    AddExperienceComponent,
    AddEducationComponent,
    PostsComponent,
    DiscussComponent
  ],
  imports: [CommonModule, DeveloperRoutingModule]
})
export class DeveloperModule {}
