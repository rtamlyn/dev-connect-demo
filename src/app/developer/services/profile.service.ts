import { Injectable } from '@angular/core'

enum ProStatus {
  'Developer',
  'Junior Developer',
  'Senior Developer',
  'Manager',
  'Student or Learning',
  'Instructor',
  'Intern',
  'Other'
}
// interface Skills [string]

export class Profile {
  name: string
  proStatus: ProStatus
  company: string
  website: string
  location: string
  skills: string[]
  githubUserName: string | null
  bitbucketUserName: string | null
  bio: string
  socialNetworks: {
    twitter?: string
    facebook?: string
    youtube?: string
    linkedIn?: string
    instagram?: string
  }
}

export const initProfile: Profile = {
  name: '',
  proStatus: ProStatus['Student or Learning'],
  company: '',
  website: '',
  location: '',
  skills: [],
  githubUserName: null,
  bitbucketUserName: null,
  bio: '',
  socialNetworks: {}
}

export const exampleProfile: Profile = {
  name: 'John Doe',
  proStatus: ProStatus.Developer,
  company: 'Microsoft',
  website: '', // TODO: Not Used yet
  location: 'Seattle, WA',
  skills: ['HTML', 'CSS', 'JavaScript', 'Python'],
  githubUserName: null,
  bitbucketUserName: null,
  bio:
    'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, distinctio tempora. Obcaecati vero ad similique numquam. Commodi incidunt quia non!',
  socialNetworks: {
    twitter: '@memorychips13',
    facebook: ''
  }
}

@Injectable()
export class ProfileService {
  getMyProfile(): Profile {
    const myProfile: Profile = initProfile
    return myProfile
  }
  constructor() {}
}
