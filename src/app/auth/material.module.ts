import { NgModule } from '@angular/core'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatButtonModule } from '@angular/material/button'
import { MatButtonToggleModule } from '@angular/material/button-toggle'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatIconModule } from '@angular/material/icon'
import { MatInputModule } from '@angular/material/input'
import { MatSnackBarModule } from '@angular/material/snack-bar'
import { MatTooltipModule } from '@angular/material/tooltip'

const matModules = [
  MatButtonModule,
  MatInputModule,
  MatIconModule,
  MatSnackBarModule,
  MatFormFieldModule,
  MatButtonToggleModule,
  MatTooltipModule,
  BrowserAnimationsModule
]

@NgModule({
  imports: matModules,
  exports: matModules
})
export class MaterialModule {}
