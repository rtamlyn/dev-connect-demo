import { TestBed, inject } from '@angular/core/testing'

import { AuthGuard } from './auth.guard'
import { RouterTestingModule } from '@angular/router/testing'
import { providers } from '../spec-mock'

describe('AuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [...providers, AuthGuard]
    })
  })

  it('should be created', inject([AuthGuard], (service: AuthGuard) => {
    expect(service).toBeTruthy()
  }))
})
