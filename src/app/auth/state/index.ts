import { createFeatureSelector, createSelector } from '@ngrx/store'
import { authFeatureKey, AuthState } from './auth.reducer'

const selectAuth = createFeatureSelector<AuthState>(authFeatureKey)

export const selectIsLoggedIn = createSelector(selectAuth, state => state.me._id !== '')
export const selectLoginFailReason = createSelector(selectAuth, state => state.loginFailReason)
export const selectSignupFailReason = createSelector(selectAuth, state => state.signupFailReason)
export const selectMe = createSelector(selectAuth, state => state.me)
export const selectReturnUrl = createSelector(selectAuth, state => state.returnUrl)
