import { Action } from '@ngrx/store'
import { User, Credentials, SignUpInfo } from '../user.model'
import { SignupFailReason } from './auth.reducer'

export enum AuthActionTypes {
  StartSignup = '[Signup] Start Signup',
  SignupFailure = '[Signup] Signup Failure',
  StartLogin = '[Auth] Start Login',
  SetReturnUrl = '[Auth] Set return url',
  LoginSuccess = '[Auth] Login Success',
  LoginFailure = '[Auth] Login Failure',
  StartLogout = '[Auth] Start Logout',
  LogoutSuccess = '[Auth] Logout Success',
  LogoutFailure = '[Auth] Logout Failure',
  ClearLoginFail = '[Auth] Clear Login Fail',
  ClearSignupFail = '[Auth] Clear Login Fail'
}

export class StartSignup implements Action {
  readonly type = AuthActionTypes.StartSignup
  constructor(public payload: SignUpInfo) {}
}

export class SignupFailure implements Action {
  readonly type = AuthActionTypes.SignupFailure
  constructor(public payload: { reason: SignupFailReason }) {}
}

export class StartLogin implements Action {
  readonly type = AuthActionTypes.StartLogin
  constructor(public payload: { credentials: Credentials }) {}
}

export class SetReturnUrl implements Action {
  readonly type = AuthActionTypes.SetReturnUrl
  constructor(public payload: string | null) {}
}

export class LoginSuccess implements Action {
  readonly type = AuthActionTypes.LoginSuccess
  constructor(public payload: { user: User }) {}
}

export class LoginFailure implements Action {
  readonly type = AuthActionTypes.LoginFailure
  constructor(public payload: { reason: string }) {}
}

export class ClearLoginFail implements Action {
  readonly type = AuthActionTypes.ClearLoginFail
  constructor() {}
}

export class ClearSignupFail implements Action {
  readonly type = AuthActionTypes.ClearSignupFail
  constructor() {}
}

export class StartLogout implements Action {
  readonly type = AuthActionTypes.StartLogout
}

export class LogoutSuccess implements Action {
  readonly type = AuthActionTypes.LogoutSuccess
}

export class LogoutFailure implements Action {
  readonly type = AuthActionTypes.LogoutFailure
  constructor(public payload: { reason: string }) {}
}

export type AuthActions =
  | StartSignup
  | LogoutSuccess
  | StartLogin
  | SetReturnUrl
  | StartLogout
  | SignupFailure
  | LogoutFailure
  | LoginSuccess
  | LoginFailure
  | ClearLoginFail
