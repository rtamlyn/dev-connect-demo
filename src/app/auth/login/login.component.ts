import { Component, OnInit, OnDestroy } from '@angular/core'
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Subscription } from 'rxjs'
import { Credentials, User } from '../user.model'
import { MatSnackBar } from '@angular/material/snack-bar'
import { SignupValidators } from '../signup/signup.validators'
import { environment } from '../../../environments/environment'

import { ActivatedRoute } from '@angular/router'
import { AuthService } from '../auth.service'

@Component({
  selector: 'dcn-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  // FIXME: convert this a fb comp maybe and check for validity before allowing submit of login
  user: User
  // isLoggedIn = false
  showPassword = false
  passwordInputType = 'password'
  hide = true
  form = new FormGroup({
    email: new FormControl('robert.tamlyn@gmail.com', [Validators.required, Validators.email]),
    password: new FormControl('Password10', [
      Validators.required,
      SignupValidators.cannotContainSpace
    ])
  })

  get email() {
    return this.form.get('email')
  }
  get password() {
    return this.form.get('password')
  }

  _subscriptions: Array<Subscription> = []

  constructor(
    private route: ActivatedRoute,
    public authService: AuthService,
    public snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    if (environment.production) {
      this.form.controls.email.setValue('')
      this.form.controls.password.setValue('')
    }
    this._subscriptions = [
      this.authService.me$.subscribe(user => {
        this.user = user
      }),
      this.authService.loginFailReason$.subscribe(reason => {
        if (reason) {
          this.openSnackBar(reason, 'dismiss')
          this.authService.clearLoginFail()
        }
      })
    ]
    let rUrl = this.route.snapshot.queryParamMap.get('returnUrl')
    if (!rUrl || rUrl.includes('signup')) rUrl = 'developers/dashboard'
    this.authService.setReturnUrl(rUrl)
  }

  ngOnDestroy() {
    this._subscriptions.map((s: Subscription) => {
      s.unsubscribe()
    })
  }

  toggleShowPassword() {
    this.showPassword = !this.showPassword
    if (this.showPassword) {
      this.passwordInputType = 'text'
    } else {
      this.passwordInputType = 'password'
    }
  }

  submitSocialLogin(provider: string) {
    console.log(`Social login not setup ${provider}`)
  }

  submitLogin() {
    const credentials: Credentials = this.form.value
    this.authService.startLogin({ credentials })
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 4000
    })
  }
}
