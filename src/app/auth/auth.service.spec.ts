import { TestBed, inject } from '@angular/core/testing'

import { AuthService } from './auth.service'
import { User } from './user.model'
import { ActivatedRoute } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'
import { HttpClient } from '@angular/common/http'
import { of } from 'rxjs'
import { StateService } from '../state/state.service'
import { Store } from '@ngrx/store'

class MockStore {
  select(_selection: string) {
    return of(null)
  }
  dispatch() {}
}

class MockStateService {
  isLoggedIn$ = of(false)
  allProducts$ = of([])
  me$ = of({})
  loginSuccess() {}
}

const PRETEND_USER: User = {
  email: 'pretend@gmail.com',
  _id: 'xyz',
  roles: ['STUDENT'],
  userName: 'Pretend',
  avatarUrl: ''
}

let loggedIn = true
class MockChatLoginService {
  setLoggedInState(b: boolean) {
    loggedIn = b
  }
}
class MockHttpClient {
  get() {
    return of(PRETEND_USER)
  }
}
class MockActivatedRoute {
  snapshot = {
    routeConfig: {
      path: 'not_a_real_path'
    },
    paramMap: {
      get: () => 'fake_id'
    }
  }
}

// let authService: AuthService

describe('AuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [
        AuthService,
        { provide: HttpClient, useClass: MockHttpClient },
        { provide: StateService, useClass: MockStateService },
        { provide: ActivatedRoute, useClass: MockActivatedRoute },
        { provide: Store, useClass: MockStore }
      ]
    })
    // authService = TestBed.get(AuthService)
  })

  it('should be created', inject([AuthService], (service: AuthService) => {
    expect(service).toBeTruthy()
    expect(loggedIn).toBe(true)
  }))
})
