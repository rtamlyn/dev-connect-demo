// Align FE/BE
export interface Credentials {
  email: string
  password: string
  userName?: string
}

export interface SignUpInfo {
  email: string
  password: string
  userName: string
  avatarUrl: string
}

export interface UserWoId {
  email: string
  userName: string
  roles: string[]
  avatarUrl: string
}

export interface User extends UserWoId {
  _id: string
}
// end Align FE/BE

export const ANONYMOUS_USER: User = {
  email: 'guest',
  _id: '', // keep this falsey
  roles: [],
  userName: 'Guest',
  avatarUrl: ''
}
